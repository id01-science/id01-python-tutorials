{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tutorial on *k-means clustering* from [Real Python](https://realpython.com/k-means-clustering-python/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %%html\n",
    "\n",
    "# <style>\n",
    "#     .container {width: 98%}\n",
    "# <\\style>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import matplotlib as mpl\n",
    "\n",
    "from sklearn.datasets import make_blobs\n",
    "from sklearn.cluster import KMeans\n",
    "from sklearn.metrics import silhouette_score\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "\n",
    "%matplotlib inline\n",
    "%config InlineBackend.figure_format = 'retina'\n",
    "\n",
    "mpl.rcParams['font.family'] = 'Liberation Sans'\n",
    "mpl.rcParams['scatter.edgecolors'] = 'k'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#  k-means"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Typical $k$-means algorithm:\n",
    "* Specify n of clusters, $k$\n",
    "* Randomly select $k$ centroids\n",
    "* *Repeat*:\n",
    "    * **Expectation**: assign each point to closest centroid\n",
    "    * **Maximisation**: compute new centroid for each cluster\n",
    "* *Until* the centroid positions do not change"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "features, true_labels = make_blobs(n_samples=200, centers=3, cluster_std=2.75, random_state=42)\n",
    "\n",
    "plt.figure()\n",
    "plt.scatter(features[:,0], features[:,1], c=true_labels, ec='black', cmap='cool');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Need feature scaling to transform numerical feature to the same scale. Here **standardisation** is used, i.e. each numerical feature is scaled to have a mean of 0 and a std deviation of 1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scaler = StandardScaler()\n",
    "scaled_features = scaler.fit_transform(features)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.scatter(scaled_features[:,0], scaled_features[:,1], c=true_labels, ec='black', cmap='cool');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kmeans = KMeans(init='random', n_clusters=3, n_init=10, max_iter=300, random_state=42)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kmeans.fit(scaled_features, sample_weight=None)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print('The lowest SSE value was:', kmeans.inertia_)\n",
    "print('\\nThe final locations of the centroids: ', *kmeans.cluster_centers_)\n",
    "print('\\nThe number of iterations required to converge: ', kmeans.n_iter_)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.scatter(scaled_features[:,0], scaled_features[:,1], c=kmeans.labels_, ec='black', cmap='cool')\n",
    "plt.scatter(*kmeans.cluster_centers_.T, marker='x', c='r')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Choosing the appropriate number of clusters $k$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The **elbow method**: run several k-means, increment $k$ with each iteration, and record the SSE:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kmeans_kwargs = {\n",
    "    \"init\": \"random\",\n",
    "    \"n_init\": 10,\n",
    "    \"max_iter\": 300,\n",
    "    \"random_state\": 42,\n",
    "}\n",
    "\n",
    "sse = []\n",
    "for k in range(1, 11):\n",
    "    kmeans = KMeans(n_clusters=k, **kmeans_kwargs)\n",
    "    kmeans.fit(scaled_features)\n",
    "    sse.append(kmeans.inertia_)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The sweet spot where the SSE vs $k$ curve starts to bend is known as the \"elbow point\", here placed at $x=3$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(range(1,11), sse, marker='o')\n",
    "plt.xlabel('$k$')\n",
    "plt.ylabel('SSE')\n",
    "plt.axvline(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The **silhouette coefficient**: quantifies cluster cohesion and separation based on relative distances between points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "silhouette_coefficients = []\n",
    "\n",
    "# Notice you start at 2 clusters for silhouette coefficient - otherwise error\n",
    "for k in range(2, 11):\n",
    "    kmeans = KMeans(n_clusters=k, **kmeans_kwargs)\n",
    "    kmeans.fit(scaled_features)\n",
    "    score = silhouette_score(scaled_features, kmeans.labels_)\n",
    "    silhouette_coefficients.append(score)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(range(2, 11), silhouette_coefficients, marker='o')\n",
    "plt.xlabel(\"$k$\")\n",
    "plt.ylabel(\"Silhouette Coefficient\")\n",
    "plt.axvline(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Advanced techniques to evaluate clustering performance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Could need these when comparing $k$-means with a density-based clustering method, e.g. DBSCAN (density-based spatial clustering of applications with noise):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.cluster import DBSCAN\n",
    "from sklearn.datasets import make_moons\n",
    "from sklearn.metrics import adjusted_rand_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "features, true_labels = make_moons(n_samples=250, noise=0.05, random_state=42)\n",
    "scaled_features = scaler.fit_transform(features)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.scatter(scaled_features[:,0], scaled_features[:,1], c=true_labels, cmap='tab10', ec='black')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Instantiate k-means and dbscan algorithms\n",
    "kmeans = KMeans(n_clusters=2)\n",
    "dbscan = DBSCAN(eps=0.3)\n",
    "\n",
    "# Fit the algorithms to the features\n",
    "kmeans.fit(scaled_features)\n",
    "dbscan.fit(scaled_features)\n",
    "\n",
    "# Compute the silhouette scores for each algorithm\n",
    "kmeans_silhouette = silhouette_score(scaled_features, kmeans.labels_).round(2)\n",
    "dbscan_silhouette = silhouette_score(scaled_features, dbscan.labels_).round (2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, constrained_layout=True)\n",
    "\n",
    "for i, l, n in zip([0,1], [kmeans.labels_, dbscan.labels_], f'k-means\\ns: {kmeans_silhouette}, DBSCAN\\ns: {dbscan_silhouette}'.split(',')):\n",
    "    ax[i].scatter(scaled_features[:,0], scaled_features[:,1], c=l, cmap='tab10', ec='black')\n",
    "    ax[i].set_title(n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Value of silhouette gives misleading indication as DBSCAN gives better labelling.\n",
    "Since we have the true labels we can use the adjusted rand index (ARI) as a better clustering metric:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ari_kmeans = adjusted_rand_score(true_labels, kmeans.labels_)\n",
    "ari_dbscan = adjusted_rand_score(true_labels, dbscan.labels_)\n",
    "\n",
    "print(f'ARI kmeans: {ari_kmeans}', f'\\nARI dbscan: {ari_dbscan}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# t-SNE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From [this Medium post](https://towardsdatascience.com/an-introduction-to-t-sne-with-python-example-5a3a293108d1) and [this notebook](https://nbviewer.jupyter.org/urls/gist.githubusercontent.com/AlexanderFabisch/1a0c648de22eff4a2a3e/raw/59d5bc5ed8f8bfd9ff1f7faa749d1b095aa97d5a/t-SNE.ipynb). Also, good resource [here](https://lvdmaaten.github.io/tsne/).\n",
    "\n",
    "t-Distributed Stochastic Neighbour Embedding:\n",
    "\n",
    "* unsupervised\n",
    "* non-linear\n",
    "* gives a feeling of how the data is arranged in ND space\n",
    "\n",
    "Giving you an idea of whether using clustering algorithms makes any sense or not. Differences with respect to PCA: t-SNE preserves only small pairwise distances or local similarities, while PCA preserves large pairwise distances to maximise variance.\n",
    "\n",
    "Algorithm:\n",
    "\n",
    "1.  Measure gaussian density of points in the neighbourhood of each point: get (local) **pairwise similarities** $P_{ij}$ between points in ND space. I.e., two points in different regions of the ND space which have a similar neighbourhood will have a high $P_{ij}$ and be considered **similar**.\n",
    "\n",
    "2. Repeat 1. using a Cauchy distribution obtaining $Q_{ij}$.\n",
    "\n",
    "3. Measure the difference between $Q_{ij}$ and $P_{ij}$ using the [Kullback-Liebler divergence](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence) (KL)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The iris dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The iris dataset is a classic and very easy multi-class classification\n",
    "dataset.\n",
    "\n",
    "| Type | Value |\n",
    "|---|---|\n",
    "| Classes  |                        3 |\n",
    "| Samples per class |              50 |\n",
    "| Samples total |                 150 |\n",
    "| Dimensionality |                  4 |\n",
    "| Features |           real, positive |\n",
    "\n",
    "Read more in the `User Guide <iris_dataset>`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from sklearn.manifold import TSNE\n",
    "from sklearn.decomposition import PCA\n",
    "\n",
    "from sklearn.datasets import load_iris\n",
    "\n",
    "iris = load_iris() # data shape: 150, 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iris.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "[plt.plot(x) for x in iris.data.T];"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_tsne = TSNE(learning_rate=100).fit_transform(iris.data)\n",
    "X_pca = PCA().fit_transform(iris.data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1,2)\n",
    "\n",
    "for a, X, name in zip(ax, [X_tsne, X_pca], 't-SNE,PCA'.split(',')):\n",
    "    a.scatter(X[:,0], X[:,1], c=iris.target, ec='black', cmap='cool')\n",
    "    a.set_title(name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The MNIST dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have a look [here](https://scikit-learn.org/stable/auto_examples/manifold/plot_lle_digits.html#sphx-glr-auto-examples-manifold-plot-lle-digits-py) too"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import load_digits\n",
    "\n",
    "# Load MNIST dataset\n",
    "mnist = load_digits()\n",
    "X, y = mnist.data / 255.0, mnist.target\n",
    "\n",
    "# Create subset and reduce to first 50 dimensions\n",
    "indices = np.arange(X.shape[0])\n",
    "np.random.shuffle(indices)\n",
    "n_train_samples = 5000\n",
    "\n",
    "X_pca = PCA(n_components=50).fit_transform(X)\n",
    "X_train = X_pca[indices[:n_train_samples]]\n",
    "y_train = y[indices[:n_train_samples]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PCA "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A fast and flexible unsupervised method for dimensionality reduction. [Source](https://jakevdp.github.io/PythonDataScienceHandbook/05.09-principal-component-analysis.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng = np.random.RandomState(1)\n",
    "data = np.dot(rng.rand(2, 2), rng.randn(2, 200)).T\n",
    "\n",
    "plt.figure()\n",
    "plt.scatter(data[:, 0], data[:, 1], c='dodgerblue')\n",
    "plt.axis('equal')\n",
    "plt.gca().set(xlabel='i', ylabel='Component 1');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By eye in the above it is clear there is a nearly linear relation between the $x$ and $y$ values. PCA attempts to learn about this relationship, which is quantified by finding a list of *principal axes* in the data. These axes are then used to describe the dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from sklearn.decomposition import PCA\n",
    "\n",
    "# init pca object\n",
    "pca = PCA(n_components=2)\n",
    "\n",
    "# fit the data\n",
    "pca.fit(data)\n",
    "\n",
    "# print some fit results\n",
    "print(f'PCA components: {pca.components_}\\n\\nPCA explained variance: {pca.explained_variance_}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can plot these numbers as vectors on the input data to gain insight on their meaning. The *components* define the direction of the vector, while the *explained variance* defined the squared-length of the vector:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def draw_vector(v0, v1, ax):\n",
    "    \n",
    "    # parameters dictating the shape of the arrows to draw\n",
    "    ap = dict(arrowstyle='->', lw=2, shrinkA=0, shrinkB=0, color='r')\n",
    "        \n",
    "    # add the arrow to the axes\n",
    "    ax.annotate('', v1, v0, arrowprops=ap)\n",
    "    \n",
    "fig, ax = plt.subplots()\n",
    "\n",
    "ax.scatter(data[:, 0], data[:, 1], c='dodgerblue')\n",
    "for length, vector in zip(pca.explained_variance_, pca.components_):\n",
    "    v = vector * 3 * np.sqrt(length)\n",
    "    draw_vector(pca.mean_, pca.mean_+v, ax)\n",
    "\n",
    "ax.set_title('Principal axes of the data')\n",
    "ax.axis('equal')\n",
    "ax.set(xlabel='x', ylabel='y');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These vectors represent the **principal axes** of the data. The length of a vector is an indication of how \"important\" that axis is in describing the distribution of the data. Specifically, the length of a vector is a measure of the variance of the data when projected onto that axis.\n",
    "\n",
    "> Reminder: **variance** $S^2$\n",
    "> $$ S^2 = \\frac{\\sum_i^n(x_i - \\bar{x})}{n-1} $$\n",
    ">\n",
    "> where $x_i$ is observation $i$ on a total of $n$, and $\\bar{x}$ is the mean value of all observations.\n",
    "\n",
    "The projection of each data point onto the principal axes are the **principal components** of the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_pca = pca.transform(data) # this is an affine transform --> translation + rotation + scaling\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "\n",
    "ax.scatter(data_pca[:, 0], data_pca[:, 1], c='dodgerblue')\n",
    "\n",
    "ax.set(xlabel='Component 1', ylabel='Component 2', title='Principal Components')\n",
    "ax.axis('equal');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dimensionality reduction by PCA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Namely, removing one or more of the smallest principal components, resulting in a lower-dimensional projection of the data that still preserves the maximal data variance. We thus fit the data using only one component:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca = PCA(n_components=1)\n",
    "pca.fit(data)\n",
    "data_pca = pca.transform(data)\n",
    "\n",
    "print(\"original shape:   \", data.shape)\n",
    "print(\"transformed shape:\", data_pca.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(data_pca)\n",
    "plt.gca().set(xlabel='i', ylabel='Component 1')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_reduced = pca.inverse_transform(data_pca)\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "\n",
    "ax.scatter(data[:, 0], data[:, 1], c='dodgerblue', alpha=.3, label='original')\n",
    "ax.scatter(data_reduced[:, 0], data_reduced[:, 1], c='r', label='reduced')\n",
    "\n",
    "ax.set(xlabel='x', ylabel='y')\n",
    "ax.axis('equal')\n",
    "ax.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The information along the least important principal axis or axes is removed, leaving only the component(s) of the data with the highest variance. This reduced-dimension dataset is often \"good enough\" to encode the most important relationship between the points."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example on a high-dimensional dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import load_digits\n",
    "\n",
    "digits = load_digits()\n",
    "\n",
    "print(digits.images.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(6, 6))  \n",
    "fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05)\n",
    "\n",
    "# plot the first 64 digits: each image is 8x8 pixels\n",
    "for i in range(64):\n",
    "    ax = fig.add_subplot(8, 8, i + 1, xticks=[], yticks=[])\n",
    "    ax.imshow(digits.images[i], cmap=plt.cm.binary, interpolation='nearest')\n",
    "    \n",
    "    # label the image with the target value\n",
    "    ax.text(0, 7, str(digits.target[i]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Project from 64 to 2 dimensions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca = PCA(2)  \n",
    "projected = pca.fit_transform(digits.data)\n",
    "\n",
    "print(digits.data.shape)\n",
    "print(projected.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the first two principal components of each point:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.scatter(projected[:, 0], projected[:, 1],\n",
    "            c=digits.target, edgecolor='none', alpha=0.8,\n",
    "            cmap=plt.cm.get_cmap('Spectral', 10))\n",
    "\n",
    "plt.xlabel('Component 1')\n",
    "plt.ylabel('Component 2')\n",
    "plt.colorbar();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca = PCA().fit(digits.data)\n",
    "\n",
    "plt.plot(np.cumsum(pca.explained_variance_ratio_))\n",
    "plt.xlabel('Number of components')\n",
    "plt.ylabel('Cumulative explained variance');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "edo.slurm",
   "language": "python",
   "name": "edo.slurm"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
