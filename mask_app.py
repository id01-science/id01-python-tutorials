import panel as pn
import bokeh
import io
import numpy as np

pn.extension()

opts = dict(width=300, height=300)

out = pn.widgets.StaticText()

def loadit(event):
    """
    - Loads file object as np.ndarray
    - Sends array to ColumnDataSource
    - Sets image glyph size to array shape
    """
#     global test, f_arr
    
    out.value = 'Loading array...'
    test = file_input.value
    
    f_arr = io.BytesIO(test) # save to buffer to preserve array shape
    arr = np.load(f_arr)

    sh0, sh1 = arr.shape
    
    source.data['z0'] = [arr]
    source.data['z1'] = [arr]

    for im in (imr, iml):
        im.glyph.dw = sh0
        im.glyph.dh = sh1
    
    out.value = 'Done!'
        
def maskit(attr, old, new):
    """
    Masks portions of source.data['z1'] (left plot) according to areas selected
    in source.data['z0'] (right plot)
    """

    H, W, X, Y = [new[key] for key in new.keys()]

    arr = source.data['z0'][0].copy() 
    for h, w, x, y in zip(H, W, X, Y):
        h, w, y, x = [int(x) for x in (h,w,y,x)]

        arr[y-h//2:y+h//2,x-w//2:x+w//2] = 0
        source.data['z1'] = [arr]
    
# init image data source
source = bokeh.models.ColumnDataSource(data=dict(z0=[], z1=[]))
        
# button to load files
file_input = pn.widgets.FileInput(accept=".npy")
file_input.param.watch(loadit, 'value')
# file_input.css_classes = ['pn-loading']

# init rectangles data source
rsource = bokeh.models.ColumnDataSource(data=dict(x=[], y=[], width=[], height=[]))

# tools
t = 'pan,wheel_zoom,reset'

# left plot
l = bokeh.plotting.figure(tools=t, tooltips=[('x', '$x'),('z','@z0')], **opts)
iml = l.image(image='z0', source=source, x=0, y=0, palette='Magma11')
l.title = 'Raw'

rect = l.rect('x', 'y', 'width', 'height', source=rsource, 
              fill_color='white', fill_alpha=.7, line_color='white', line_width=2)
select = bokeh.models.BoxEditTool(renderers=[rect])
l.add_tools(select)

# right plot
r = bokeh.plotting.figure(tools=t, tooltips=[('z','@z1')], x_range=l.x_range, y_range=l.y_range, **opts)
imr = r.image(image='z1', source=source, x=0, y=0, palette='Magma11')
r.title = 'Masked'

# call maskit when rsource data changes
rsource.on_change('data', maskit)

def update_filebuffer():
    """
    Saves mask to file object which can then be dumped to 
    disk through a panel widget
    """
    mask = source.data['z1'][0] > 0
    
    fobj = io.BytesIO()
    np.save(fobj, mask)
    fobj.seek(0)
    
    return fobj

download = pn.widgets.FileDownload(callback=update_filebuffer, filename='testout.npy')

grid = pn.Row(pn.Column(file_input, download, out), pn.Row(l,r))
grid.servable()
