{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Introduction-to-morphological-image-processing\" data-toc-modified-id=\"Introduction-to-morphological-image-processing-1\"><span class=\"toc-item-num\">1&nbsp;&nbsp;</span>Introduction to morphological image processing</a></span><ul class=\"toc-item\"><li><span><a href=\"#Binary-morphology\" data-toc-modified-id=\"Binary-morphology-1.1\"><span class=\"toc-item-num\">1.1&nbsp;&nbsp;</span>Binary morphology</a></span><ul class=\"toc-item\"><li><span><a href=\"#Erosion-&amp;-Dilation\" data-toc-modified-id=\"Erosion-&amp;-Dilation-1.1.1\"><span class=\"toc-item-num\">1.1.1&nbsp;&nbsp;</span>Erosion &amp; Dilation</a></span></li><li><span><a href=\"#Compound-operations:-opening-&amp;-closing\" data-toc-modified-id=\"Compound-operations:-opening-&amp;-closing-1.1.2\"><span class=\"toc-item-num\">1.1.2&nbsp;&nbsp;</span>Compound operations: opening &amp; closing</a></span></li></ul></li><li><span><a href=\"#Grayscale-operations\" data-toc-modified-id=\"Grayscale-operations-1.2\"><span class=\"toc-item-num\">1.2&nbsp;&nbsp;</span>Grayscale operations</a></span><ul class=\"toc-item\"><li><span><a href=\"#Reconstruction\" data-toc-modified-id=\"Reconstruction-1.2.1\"><span class=\"toc-item-num\">1.2.1&nbsp;&nbsp;</span>Reconstruction</a></span></li></ul></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to morphological image processing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A technique based on the concepts of *mathematical morphology* (MM), which analyses geometrical structures based on set theory and topology. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib as mpl\n",
    "\n",
    "from skimage import data, morphology, img_as_float, measure, filters, transform, exposure, restoration, feature\n",
    "\n",
    "%config InlineBackend.figure_format = 'retina'\n",
    "%matplotlib Inline\n",
    "\n",
    "mpl.rcParams['image.cmap'] = 'Greys'    \n",
    "mpl.rcParams['font.size'] = 9\n",
    "mpl.rcParams['font.family'] = 'Liberation Sans'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binary morphology"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The underlying principles of MM are best understood in the context of binary images, i.e. those where a pixel is either equal to 1 or 0. Below we generate such an image via the `skimage.data` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "blobs = data.binary_blobs(seed=10)\n",
    "\n",
    "plt.figure(figsize=(4,3))\n",
    "plt.imshow(blobs)\n",
    "plt.colorbar();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we define a *structuring element* (SE). This is a kernel which will be convoluted with the image to be analysed - here the \"blobs\" plotted above. We define an SE as a disk of radius equal to 5 pixels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SE = morphology.selem.disk(5)\n",
    "\n",
    "plt.figure(figsize=(4,3))\n",
    "plt.imshow(SE)\n",
    "plt.colorbar();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The centre of the SE will be placed on each pixel of the \"blobs\" image. The value of this image pixel will be compared to that of those pixels falling within the rest of the SE. A chosen mathematical operation then outputs a result binary image based on this comparison."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Erosion & Dilation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simplest of these operations are *erosion* and *dilation*. \n",
    "\n",
    "Erosion yields an image containing only features which \"fit\" in the chosen SE: anything else is given a value of 0. Features (here, \"blobs\") in the resulting image thus appear \"thinner\". This operation can be used to remove small isolated features from an image.\n",
    "\n",
    "Dilation does the opposite: it produces a new binary image with ones at all locations of the SE origin at which the SE hits a pixel equal to 1 in the original image. Thus, features in the resulting image appear \"thickened\".\n",
    "\n",
    "\n",
    "Erosion of the dark blue square by a disk, yielding the light blue square | Dilation  of the dark blue square by a disk, yielding the light blue square\n",
    ":--------:| :--------:\n",
    "![Erosion](Erosion.png) | ![Dilation](Dilation.png)\n",
    "\n",
    "The following demonstrates this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# erosion\n",
    "blobs_eroded = morphology.binary_erosion(blobs, selem=SE)\n",
    "\n",
    "# dilation\n",
    "blobs_dilated = morphology.binary_dilation(blobs, selem=SE)\n",
    "\n",
    "# morphological gradient: logical XOR , dilation-erosion\n",
    "morpho_gradient = blobs_dilated ^ blobs_eroded  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "imgs = [blobs, blobs_eroded, blobs_dilated, morpho_gradient]\n",
    "names = 'Data,Eroded,Dilated,Morphological Gradient'.split(',')\n",
    "\n",
    "fig, axes = plt.subplots(2,4, constrained_layout=True, figsize=(9,4.5))\n",
    "fig.suptitle('Using a disk of 5px radius')\n",
    "\n",
    "for ax, topl, name in zip(axes[0,:], imgs, names):\n",
    "    ax.imshow(topl)\n",
    "    ax.set_title(name)\n",
    "\n",
    "for ax, topl in zip(axes[1,:],imgs):\n",
    "    ax.imshow(topl[20:110,147:247])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above we have also included the *morphological gradient*: the boolean subtraction of the eroded image from the dilated image, yielding the boundaries of the original features."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compound operations: opening & closing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most morphological functions are a combination of erosion, dilation, and simple operations from set theory (interesections, unions, ...). \n",
    "\n",
    "The **opening** of an image by a SE consists of an erosion followed by a dilation. This will \"open up\" gaps between features separated by thin bridges of pixels.\n",
    "\n",
    "The **closing** of an image is the opposite, i.e. a dilation followed by an erosion. It will fill holes within features.\n",
    "\n",
    "Opening | Closing\n",
    ":------:| :-----:\n",
    "![Opening](Opening.png) | ![Closing](Closing.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The effect of opening and closing is apparent if applied to our \"blobs\" image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SE\n",
    "radius = 20\n",
    "selem = morphology.selem.disk(radius)\n",
    "\n",
    "# opening\n",
    "blobs_opened = morphology.binary_opening(blobs, selem=selem)\n",
    "\n",
    "# closing\n",
    "blobs_closed = morphology.binary_closing(blobs, selem=selem)\n",
    "\n",
    "# plot\n",
    "imgs = [blobs, blobs_opened, blobs_closed]\n",
    "names = 'Data,Opened,Closed'.split(',')\n",
    "\n",
    "fig, axes = plt.subplots(2,3, constrained_layout=True, figsize=(7,4.5))\n",
    "fig.suptitle('Using a disk of {}px radius'.format(radius))\n",
    "\n",
    "for ax, topl, name in zip(axes[0,:], imgs, names):\n",
    "    ax.imshow(topl)\n",
    "    ax.set_title(name)\n",
    "\n",
    "for ax, topl in zip(axes[1,:],imgs):\n",
    "    ax.imshow(topl[20:110,147:247])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Grayscale operations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mpl.rcParams['image.cmap'] = 'turbo'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same principles described above can be applied to grayscale images, i.e. those where each pixel can have a value from 0 to N. Normally to be treated by MM these images have to be normalised in the [0,1] range. This is the case for `skimage`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reconstruction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An important greayscale MM opration is **reconstruction by dilation**. This works in the following way:\n",
    "* A \"seed\" image is produced by subtracting a certain value to the original image. \n",
    "* The dilation of the seed image is performed.\n",
    "* The dilated image is dilated again iteratively until the size of the dilated features matches that of the original image.\n",
    "\n",
    "The figure below illustrates this process:\n",
    "\n",
    "![test](https://fr.mathworks.com/help/images/morph5.gif)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we perform such procedure and subtract the reconstructed image from the original, thus isolating the features from the background. The latter is known as the *h-dome* of the original image. The synthetic image consists of two noisy gaussians."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# make image\n",
    "synth = np.zeros((100,100))\n",
    "synth[50,60] = 100\n",
    "synth[50,20] = 200\n",
    "synth = filters.gaussian(synth, sigma=10)\n",
    "\n",
    "# add noise\n",
    "noise = np.random.normal(1, 1e-2, size=synth.size)\n",
    "noised_img = synth + noise.reshape(synth.shape) \n",
    "\n",
    "# reconstruct noisy img\n",
    "seed = noised_img - 0.08\n",
    "dilated = morphology.reconstruction(seed, noised_img, selem=morphology.selem.disk(5), method='dilation')\n",
    "hdome = noised_img - dilated\n",
    "\n",
    "# plot\n",
    "fig, ax = plt.subplots(2,2, figsize=(6,6))\n",
    "ax = ax.flatten()\n",
    "\n",
    "ax[0].imshow(noised_img)\n",
    "ax[0].set_title('Data')\n",
    "\n",
    "ax[1].imshow(dilated)\n",
    "ax[1].set_title('Dilated')\n",
    "\n",
    "ax[2].imshow(hdome)\n",
    "ax[2].set_title('Data - dilated: hdome')\n",
    "\n",
    "ax[3].plot(noised_img[50], label='data')\n",
    "ax[3].plot(dilated[50], label='dilated')\n",
    "ax[3].plot(seed[50], label='seed')\n",
    "ax[3].legend()\n",
    "\n",
    "for a in ax[:3]:\n",
    "    a.axhline(50, c='r', ls='--')\n",
    "    \n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The h-dome image can be used for further processing, such as finding the position of regional maxima:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.ndimage import center_of_mass\n",
    "\n",
    "# threshold\n",
    "hdome_ts = hdome > .05\n",
    "\n",
    "# close thresholded image --> fill gaps if there\n",
    "hdome_ts_closed = morphology.closing(hdome_ts, selem=morphology.selem.disk(5))\n",
    "\n",
    "# label and get properties of labelled regions\n",
    "labels = measure.label(hdome_ts_closed)\n",
    "region_props = measure.regionprops(labels)\n",
    "\n",
    "# plot\n",
    "fig, ax = plt.subplots(1,3, figsize=(8,3), constrained_layout=True, sharex=True, sharey=True)\n",
    "\n",
    "ax[0].imshow(hdome_ts_closed)\n",
    "ax[1].imshow(labels, cmap='viridis')\n",
    "ax[2].imshow(synth)\n",
    "[ax[2].scatter(r.centroid[1], r.centroid[0], c='r', marker='x') for r in region_props]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note how the procedure yields the correct centroid for the **original** noise-free image with half a pixel."
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": false,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
